const initState = {
  projects: [
    {id: '1', title: 'Title1', content: 'content goes here'},
    {id: '2', title: 'Title2', content: 'content goes here'},
    {id: '3', title: 'Title3', content: 'content goes here'}
  ]
}

const projectReducer = (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_PROJECT':
      console.log('created project', action.project);
      return state;
    case 'CREATE_PROJECT_ERROR':
      console.log('error', action.err);
      return state;
    default:
      return state;
  }
}

export default projectReducer;
