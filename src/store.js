import { applyMiddleware, createStore, combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import { firestoreReducer } from 'redux-firestore';
import authReducer from './reducers/authReducer';
import projectReducer from './reducers/projectReducer';
import thunk from 'redux-thunk';
import 'firebase/firestore';
import 'firebase/auth';

const reducer = combineReducers({
  auth: authReducer,
  project: projectReducer,
  firebase: firebaseReducer,
  firestore: firestoreReducer,
});

const initialState = {}

export default () => {
  return createStore(
    reducer,
    initialState,
    applyMiddleware(thunk)
  )
};
