import React from 'react';
import ReactDOM from 'react-dom';
import createReduxStore from './store';
import firebase from 'firebase/app';
import { config } from './firebaseConfig';
import { createFirestoreInstance } from 'redux-firestore';
import { Provider } from 'react-redux';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import App from './App';

export const rrfConfig = {
  userProfile: 'users',
  // attachAuthIsReady: true,
  useFirestoreForProfile: true
}

// Initialize firebase instance
firebase.initializeApp(config);

// Initialize other services on firebase instance
firebase.auth();
firebase.firestore().settings({});

// Export authRef and provider
export const authRef = firebase.auth();
export const provider = new firebase.auth.GoogleAuthProvider();

// Create redux store
const store = createReduxStore();

export const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

console.log(store);
// render react app
ReactDOM.render(
  <Provider store={store}>
		<ReactReduxFirebaseProvider {...rrfProps}>
			<App />
		</ReactReduxFirebaseProvider>
  </Provider>
  , document.getElementById('root'));
