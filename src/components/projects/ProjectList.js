import React, { Component } from 'react';
import ProjectSummary from './ProjectSummary';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase';

class ProjectList extends Component {

  renderProjects() {
    return this.props.projects.map(project => {
      return (
        <ProjectSummary project={project} key={project.id} />
      );
    });
  }

  render() {
    return (
      <div className="project-list section">
        {this.renderProjects()}
      </div>
    );
  }
}
// const ProjectList = ({ projects }) => {
//
//   return (
//     <div className="project-list section">
//       { projects && projects.map(project => {
//         return (
//           <ProjectSummary project={project} key={project.id} />
//         );
//       })}
//     </div>
//   )
// }

function mapStateToProps(state) {
  const projectListData = state.firestore.ordered["projectListData"]

  let projects = [];

  if (isLoaded(projectListData) && !isEmpty(projectListData)) {
    projects = projectListData;
  }
  return { projects }
}

export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'projects',
      storeAs: 'projectListData'
    }
  ])
)(ProjectList);
