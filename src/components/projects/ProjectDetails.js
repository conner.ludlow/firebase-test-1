import React from 'react';

const ProjectDetails = (props) => {
  const id = props.match.params.id;
  return (
    <div className="material section project-details container">
      <div className ="card">
        <div classsName="card-content">
          <span className='card-title'>Project Title - {id}</span>
          <p>lorem ipsum</p>
        </div>
        <div className="action grey lighten-4 grey-text">
          <div>Posted By Me</div>
          <div>2 sept</div>
        </div>
      </div>
    </div>
  );
}

export default ProjectDetails;
